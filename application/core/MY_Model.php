<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class  MY_Model  extends  CI_Model  {

    function __construct()
    {
        parent::__construct();
    }
    
    public function get_data($select = '*', $table, $where){
	 	$this->db->select($select)
	 		->from($table)
	 		->where($where);

	 	$query = $this->db->get();
	 	return $query->row_object();
	}

    public function select_from($select = '*', $table, $order_by = 'id', $order='ASC', $limit = FALSE , $start = 0){
	 	if($limit!=FALSE) {
	 		$this->db->limit($limit,$start);
	 	}
	 	$this->db->select($select)
	 		->from($table)
	 		->order_by($order_by,$order);

	 	$query = $this->db->get();
	 	return $query->result_object();
	}
	public function get_all($select = '*', $table, $where){
	 	$this->db->select($select)
	 		->from($table)
	 		->where($where);

	 	$query = $this->db->get();
	 	return $query->result_object();
	}
}