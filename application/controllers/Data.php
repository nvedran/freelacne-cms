<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends MY_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('data_m');
    }


    public function index()
	{
		if(!user_rules('admin')) {
        	redirect('/','location');
        }
		$data['title'] = $this->lang->line('db_list');


		$data['users'] = $this->data_m->data_hosting();

		$this->data['content'] = $this->load->view('back/parts/data_admin.php',$data,TRUE);
		$this->load->view('back/index',$this->data);

	}

	public function add_data()
	{
		if(!user_rules('admin')) {
        	redirect('/','location');
        }
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
					array(	
			                'field' => 'database',
			                'label' => $this->lang->line('database'),
			                'rules' => 'trim|required|is_unique[data.name]'
			        ),
			        array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'trim|required|required'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'trim|required'
			        ),
			        array(
			                'field' => 'hosting_id',
			                'label' => $this->lang->line('hosting_id'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {    
	        	//Insert customers data   
				$db_data = array(
						'name' => $this->input->post('database'),
				    	'username' => $this->input->post('username'),
				    	'password' => $this->input->post('password')
				);

				$this->db->insert('data', $db_data);
				$db_id = $this->db->insert_id();
				//var_dump($db_id);

				// Link DB to specific Hosting admin

				$db_hosting = array(
				       'data_id' => $db_id,
				       'hosting_id' => $this->input->post('hosting_id')
				);
				$this->db->insert('data_hosting', $db_hosting);

				

				redirect('/data/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_db');
		$this->data['content'] = $this->load->view('back/parts/add_data',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}


	public function edit_data($id)
	{
		
		if(!user_rules('admin')) {
        	redirect('/','location');
        }

		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
			        array(	
			                'field' => 'database',
			                'label' => $this->lang->line('database'),
			                'rules' => 'trim|required'
			        ),
			        array(
			                'field' => 'username',
			                'label' => $this->lang->line('username'),
			                'rules' => 'trim|required|required'
			        ),
			        array(
			                'field' => 'password',
			                'label' => $this->lang->line('password'),
			                'rules' => 'trim|required'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {    
	        	//Insert customers data   
				$db_data = array(
						'name' => $this->input->post('database'),
				    	'username' => $this->input->post('username'),
				);
				if($this->input->post('password')!=''){
					$db_data['password'] = $this->input->post('password');
				}

				$this->db->where('id', $id);
				$this->db->update('data', $db_data);
		
				redirect('/data/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_db');

		$select = array('id','name','username','password');
		$where = array('id'=>$id);
	    $data['database'] = $this->user_m->get_data($select, 'data', $where);

	    if($data['database'] == NULL){
	    	redirect('/data/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_data',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function data_list($id)
	{
		if(!user_rules('admin')) {
        	redirect('/','location');
        }
		$data['title'] = $this->lang->line('db_list');

	    $data['databases'] = $this->data_m->users_db($id);

	    if($data['databases'] == NULL){
	    	redirect('/data/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/data_user_list.php',$data,TRUE);
		$this->load->view('back/index',$this->data);

	}

	public function delete_data($id) {

		if(!user_rules('admin')) {
        	redirect('/','location');
        }

		$this->db->where(array('data_id'=>$id));
		$this->db->delete('data_hosting');

		$this->db->where(array('id'=>$id));
		$this->db->delete('data');

		redirect('/data/', 'location');
	}


}