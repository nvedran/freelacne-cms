<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domains extends MY_Controller {


	function __construct() {
        parent::__construct();
        $this->load->model('hosting_m');
        if(!user_rules('admin')) {
        	redirect('/','location');
        }
        
    }
	public function index()
	{
		$data['title'] = $this->lang->line('domain_list');

		$select = array('id', 'name', 'hosting_id', 'start','end');
		$data['domains'] = $this->hosting_m->select_from($select,'domains','end');

		$this->data['content'] = $this->load->view('back/parts/domains_admin',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}
	public function add_domain()
	{
		
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
					array(
			                'field' => 'name',
			                'label' => $this->lang->line('name'),
			                'rules' => 'required|trim|is_unique[domains.name]'
			        ),
			        array(
			                'field' => 'hosting_id',
			                'label' => $this->lang->line('username'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'start',
			                'label' => $this->lang->line('start'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'end',
			                'label' => $this->lang->line('end'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'price',
			                'label' => $this->lang->line('price'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'main_domain',
			                'label' => $this->lang->line('main_domain'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				 $domain_data = array(
				       'name' => $this->input->post('name'),
				       'hosting_id' => $this->input->post('hosting_id'),
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end'),
				       'main_domain' => $this->input->post('main_domain')

				);
				$this->db->insert('domains', $domain_data);

				redirect('/domains/', 'location');
				
	        }
	    }


	    $data['title'] = $this->lang->line('add_hosting');


		$this->data['content'] = $this->load->view('back/parts/add_domain',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	public function edit_domain($id)
	{
		$data['main_error'] = FALSE;

		if($_POST) {
			$rules = array(
					array(
			                'field' => 'name',
			                'label' => $this->lang->line('name'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'hosting_id',
			                'label' => $this->lang->line('username'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'start',
			                'label' => $this->lang->line('start'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'end',
			                'label' => $this->lang->line('end'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'price',
			                'label' => $this->lang->line('price'),
			                'rules' => 'required|trim'
			        ),
			        array(
			                'field' => 'main_domain',
			                'label' => $this->lang->line('main_domain'),
			                'rules' => 'required|trim'
			        )
			);

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE)
	        {
	        	$data['main_error'] = $this->lang->line('main_error');
	        }
	        else
	        {        
				 $domain_data = array(
				       'name' => $this->input->post('name'),
				       'hosting_id' => $this->input->post('hosting_id'),
				       'start' => $this->input->post('start'),
				       'end' => $this->input->post('end'),
				       'price' => $this->input->post('price'),
				       'main_domain' => $this->input->post('main_domain')

				);
				//var_dump($domain_data);
				$this->db->where('id', $id);
				$this->db->update('domains', $domain_data);

				redirect('/domains/', 'location');
				
	        }
	    }

		$data['title'] = $this->lang->line('edit_domain');

		$select = array('id','name','hosting_id','start','end','price','main_domain');
		$where = array('id'=>$id);
	    $data['domains'] = $this->hosting_m->get_data($select, 'domains', $where);
	    
	    if($data['domains'] == NULL){
	    	redirect('/domains/', 'location', 302);
	    }

		$this->data['content'] = $this->load->view('back/parts/edit_domain',$data,TRUE);
		$this->load->view('back/index',$this->data);
	}

	

}
