<?php 

class user_m extends MY_Model  {

    function __construct() {
        parent::__construct();
    }

	public function loged_in($where){
	 	$this->db->select('id, username, user_group')
	 		->from('users')
	 		->where($where);
	 	$query = $this->db->get();
	 	return $query->row_object();
	}
	
}