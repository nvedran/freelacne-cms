<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('db_created');?></th>
					<th><?php echo $this->lang->line('db_numb');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($users as $user):?>
				<tr>
					<th><?php echo $user->id;?></th>
					<td><?php echo $user->username;?></td>
					<td><?php echo db_limit($user->id);?></td>
					<td><?php echo $user->db_numb;?></td>
					<td>
						<?php if (db_limit($user->id) > 0) :?>
							<a class="btn btn-default" href="<?php echo base_url("data/data_list/$user->id");?>" role="button">
								<?php echo $this->lang->line('db_view');?>
							</a>
						<?php endif;?>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/data/add_data');?>" class="btn btn-success" role="button">
					<?php echo $this->lang->line('add_db');?>
				</a>
			</div>
		</div>
	</div>
</div>


