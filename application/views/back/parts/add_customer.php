<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('customers/add_customer'); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('customers_data');?></legend>
					
					<div class="form-group <?php echo form_error('name')? 'has-error': '';?>"> 
						<label for="name" class="col-lg-2 control-label"><?php echo $this->lang->line('first_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="<?php echo form_error('name')? form_error('name'): $this->lang->line('first_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('surname')? 'has-error': '';?>">
					  	<label for="surname" class="col-lg-2 control-label"><?php echo $this->lang->line('last_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="surname" name="surname" value="<?php echo set_value('surname'); ?>" placeholder="<?php echo form_error('surname')? form_error('surname'): $this->lang->line('last_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('email')? 'has-error': '';?>">
					  	<label for="email" class="col-lg-2 control-label"><?php echo $this->lang->line('email');?></label>
					  	<div class="col-lg-10">
				    		<input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="<?php echo form_error('email') ? form_error('email') : $this->lang->line('email');?>">
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('ftp_data');?></legend>
					<div class="form-group <?php echo form_error('domain')? 'has-error': '';?>">
						<label for="domain" class="col-lg-2 control-label"><?php echo $this->lang->line('domain');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="domain" name="domain" value="<?php echo set_value('domain'); ?>" placeholder="<?php echo form_error('domain')? form_error('domain'): $this->lang->line('domain');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="<?php echo form_error('username')? form_error('username'): $this->lang->line('username');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password" name="password" value="<?php echo set_value('password')? set_value('password'): get_random_password(); ?>">
						</div>
					</div>
					<legend><?php echo $this->lang->line('database_data');?></legend>
					<div class="form-group <?php echo form_error('database')? 'has-error': '';?>">
						<label for="database" class="col-lg-2 control-label"><?php echo $this->lang->line('db_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="database" name="database" value="<?php echo set_value('database'); ?>" placeholder="<?php echo form_error('database')? form_error('database'): $this->lang->line('db_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('db_username')? 'has-error': '';?>">
						<label for="db_username" class="col-lg-2 control-label"><?php echo $this->lang->line('db_user');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="db_username" name="db_username" value="<?php echo set_value('db_username'); ?>" placeholder="<?php echo form_error('db_username')? form_error('db_username'): $this->lang->line('db_user');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="db_password <?php echo form_error('db_password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="db_password" name="db_password" value="<?php echo set_value('db_password')? set_value('db_password'): get_random_password(); ?>">
						</div>
					</div>
					<legend><?php echo $this->lang->line('company_data');?></legend>
					
					<div class="form-group">
					  	<label for="company_name" class="col-lg-2 control-label"><?php echo $this->lang->line('company_name');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo set_value('company_name'); ?>" placeholder="<?php echo $this->lang->line('company_name');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_address" class="col-lg-2 control-label"><?php echo $this->lang->line('company_address');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_address" name="company_address" value="<?php echo set_value('company_address'); ?>" placeholder="<?php echo $this->lang->line('company_address');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_phone" class="col-lg-2 control-label"><?php echo $this->lang->line('company_phone');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_phone" name="company_phone" value="<?php echo set_value('company_phone'); ?>" placeholder="<?php echo $this->lang->line('company_phone');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_desc" class="col-lg-2 control-label"><?php echo $this->lang->line('company_desc');?></label>
					  	<div class="col-lg-10">
					    	<textarea class="form-control" rows="3" id="company_desc" name="company_desc" placeholder="<?php echo $this->lang->line('company_desc');?>"><?php echo set_value('company_desc'); ?></textarea>
					  	</div>
					</div>
					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
