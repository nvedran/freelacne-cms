<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-12">
		  <table class="table table-striped table-hover">
		    <thead>
				<tr>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($users as $user):?>
				<tr>
					<td><?php echo $user->username;?></td>
					<td>
						<?php if(isset($_GET['token'])): ?>
						<a class="btn btn-default" href="<?php echo base_url("tasks/get_tasks/?token=" . $_GET['token'] . "&board=$user->trello");?>" role="button">
							<?php echo $this->lang->line('tasks_list');?>
						</a>
						<?php endif;?>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		  </table>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<a href="javascript:void(0)" class="btn btn-success" role="button" onclick="AuthenticateTrello()" id="cTrello">
							 Connect With Trello
						</a>
						<a href="javascript:void(0)" class="btn btn-danger" role="button" onclick="DeauthenticateTrello()" id="dTrello" style="display:none;">
							 Disconnect from Trello
						</a>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>