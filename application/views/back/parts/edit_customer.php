<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url("customers/edit_customer/$customer->id"); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('customers_data');?></legend>
					
					<div class="form-group <?php echo form_error('name')? 'has-error': '';?>"> 
						<label for="name" class="col-lg-2 control-label"><?php echo $this->lang->line('first_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="name" name="name" value="<?php echo $customer->owner_name; ?>" placeholder="<?php echo form_error('name')? form_error('name'): $this->lang->line('first_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('surname')? 'has-error': '';?>">
					  	<label for="surname" class="col-lg-2 control-label"><?php echo $this->lang->line('last_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="surname" name="surname" value="<?php echo $customer->owner_surname; ?>" placeholder="<?php echo form_error('surname')? form_error('surname'): $this->lang->line('last_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('email')? 'has-error': '';?>">
					  	<label for="email" class="col-lg-2 control-label"><?php echo $this->lang->line('email');?></label>
					  	<div class="col-lg-10">
				    		<input type="text" class="form-control" id="email" name="email" value="<?php echo $customer->email; ?>" placeholder="<?php echo form_error('email') ? form_error('email') : $this->lang->line('email');?>">
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('company_data');?></legend>
					<div class="form-group">
					  	<label for="company_name" class="col-lg-2 control-label"><?php echo $this->lang->line('company_name');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_name" name="company_name" value='<?php echo $customer->company_name ? $customer->company_name : set_value('company_name'); ?>' placeholder="<?php //echo $this->lang->line('company_name');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_address" class="col-lg-2 control-label"><?php echo $this->lang->line('company_address');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_address" name="company_address" value='<?php echo $customer->company_address ? $customer->company_address : set_value('company_address'); ?>' placeholder="<?php echo $this->lang->line('company_address');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_phone" class="col-lg-2 control-label"><?php echo $this->lang->line('company_phone');?></label>
					  	<div class="col-lg-10">
					    	<input type="text" class="form-control" id="company_phone" name="company_phone" value='<?php echo $customer->company_phone ? $customer->company_phone : set_value('company_phone'); ?>' placeholder="<?php echo $this->lang->line('company_phone');?>">
					  	</div>
					</div>
					<div class="form-group">
					  	<label for="company_desc" class="col-lg-2 control-label"><?php echo $this->lang->line('company_desc');?></label>
					  	<div class="col-lg-10">
					    	<textarea class="form-control" rows="3" id="company_desc" name="company_desc" placeholder='<?php echo $this->lang->line('company_desc');?>'><?php echo $customer->company_desc ? $customer->company_desc : set_value('company_desc'); ?></textarea>
					  	</div>
					</div>
					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
