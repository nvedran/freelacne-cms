<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('domain');?></th>
					<th><?php echo $this->lang->line('end');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($domains as $domain):?>
				<tr class="<?php echo expire($domain->end);?>">
					<th><?php echo $domain->id;?></th>
					<td><?php echo $domain->name;?></td>
					<td><?php echo date("d.m.Y", strtotime($domain->end));?></td>
					<td><a class="btn btn-default" href="<?php echo base_url("domains/edit_domain/$domain->id");?>" role="button"><?php echo $this->lang->line('edit');?></a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/domains/add_domain');?>" class="btn btn-success " role="button">
					<?php echo $this->lang->line('add_domain');?>
				</a>
			</div>
		</div>
	</div>
</div>
