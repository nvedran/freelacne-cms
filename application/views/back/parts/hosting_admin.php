<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('domain');?></th>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('hp_name');?></th>
					<th><?php echo $this->lang->line('end');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($hosting as $hosting):?>
				<tr class="<?php echo expire($hosting->end);?>">
					<th><?php echo $hosting->id;?></th>
					<td><?php echo $hosting->domain;?></td>
					<td><?php echo $hosting->username;?></td>
					<td><?php echo $hosting->package;?></td>
					<td><?php echo date("d.m.Y", strtotime($hosting->end));?></td>
					<td><a class="btn btn-default" href="<?php echo base_url("hosting/edit_hosting/$hosting->id");?>" role="button"><?php echo $this->lang->line('edit');?></a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/hosting/add_hosting');?>" class="btn btn-success " role="button">
					<?php echo $this->lang->line('add_hosting');?>
				</a>
				<a href="<?php echo base_url('/hosting/packages');?>" class="btn btn-primary " role="button">
					<?php echo $this->lang->line('packages_list');?>
				</a>
			</div>
		</div>
	</div>
</div>
