<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('username');?></th>
					<th><?php echo $this->lang->line('email');?></th>
					<th><?php echo $this->lang->line('user_group');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($users as $user):?>
				<tr>
					<th><?php echo $user->id;?></th>
					<td><?php echo $user->username;?></td>
					<td><?php echo $user->email;?></td>
					<td><?php echo $user->user_group;?></td>
					<td>
						<a class="btn btn-default" href="<?php echo base_url("users/edit_user/$user->id");?>" role="button">
							<?php echo $this->lang->line('edit');?>
						</a>
						<a class="btn btn-danger" href="<?php echo base_url("users/delete_user/$user->id");?>" role="button">
							<?php echo $this->lang->line('delete');?>
						</a>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/users/add_user');?>" class="btn btn-success" role="button">
					<?php echo $this->lang->line('add_user');?>
				</a>
			</div>
		</div>
	</div>
</div>


