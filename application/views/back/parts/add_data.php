<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
		<div class="well">
			<?php if(!empty($main_error)):?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $main_error;?>
				</div>
				
			<?php endif;?>
            <form class="form-horizontal" method="post" action="<?php echo base_url('data/add_data'); ?>">
				<fieldset>
					<legend><?php echo $this->lang->line('ftp_data');?></legend>
					<div class="form-group <?php echo form_error('hosting_id')? 'has-error': '';?>">
					  	<label for="hosting_id" class="col-lg-2 control-label"><?php echo $this->lang->line('username');?></label>
					  	<div class="col-lg-10">
					  		<select class="form-control" name="hosting_id" id="hosting_id" value="<?php echo set_value('hosting_id'); ?>">
					  			<?php foreach (db_hosting() as $id => $username):?>
					  				<option value="<?php echo $id;?>" <?php echo $id == set_value('hosting_id') ? 'selected="selected"':'';?>><?php echo $username;?></option>
				                <?php endforeach;?>
					  		</select>
					  	</div>
					</div>
					<legend><?php echo $this->lang->line('database_data');?></legend>
					<div class="form-group <?php echo form_error('database')? 'has-error': '';?>">
						<label for="database" class="col-lg-2 control-label"><?php echo $this->lang->line('db_name');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="database" name="database" value="<?php echo set_value('database'); ?>" placeholder="<?php echo form_error('database')? form_error('database'): $this->lang->line('db_name');?>">
						</div>
					</div>
					<div class="form-group <?php echo form_error('username')? 'has-error': '';?>">
						<label for="username" class="col-lg-2 control-label"><?php echo $this->lang->line('db_user');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="<?php echo form_error('username')? form_error('username'): $this->lang->line('db_user');?>">
						</div>
					</div>
					<div class="form-group">
					  	<label for="password <?php echo form_error('password')? 'has-error': '';?>" class="col-lg-2 control-label"><?php echo $this->lang->line('password');?></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="password" name="password" value="<?php echo set_value('password')? set_value('password'): get_random_password(); ?>">
						</div>
					</div>

					<legend></legend>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
					    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#cancel-confirm"><?php echo $this->lang->line('cancel');?></button>
					    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
