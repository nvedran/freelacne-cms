<div class="row">
	<div class="col-md-12">

		<div class="page-header">
			<h2><?php echo $title;?></h2>
		</div>
	    <table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th><?php echo $this->lang->line('hp_name');?></th>
					<th><?php echo $this->lang->line('price');?></th>
					<th><?php echo $this->lang->line('db_numb');?></th>
					<th><?php echo $this->lang->line('domain_numb');?></th>
					<th><?php echo $this->lang->line('actions');?></th>
				</tr>
      		</thead>
			<tbody>
				<?php foreach($packages as $package):?>
				<tr>
					<th><?php echo $package->id;?></th>
					<td><?php echo $package->name;?></td>
					<td><?php echo $package->price;?></td>
					<td><?php echo $package->db_numb;?></td>
					<td><?php echo $package->domains;?></td>
					<td><a class="btn btn-default" href="<?php echo base_url("hosting/edit_package/$package->id");?>" role="button"><?php echo $this->lang->line('edit');?></a></td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<div class="panel panel-default">
			<div class="panel-body">
				<a href="<?php echo base_url('/hosting/add_package');?>" class="btn btn-success " role="button">
					<?php echo $this->lang->line('add_hosting_package');?>
				</a>
			</div>
		</div>
	</div>
</div>
