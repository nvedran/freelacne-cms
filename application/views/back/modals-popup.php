<!-- Modal -->
<div class="modal fade" id="cancel-confirm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('data_alert');?></h4>
      </div>
      <div class="modal-body">
	   	<?php echo $this->lang->line('data_alert_body');?>
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
	    <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" type="button" class="btn btn-danger"><?php echo $this->lang->line('confirm');?></a>
	  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->