//Trello.deauthorize();
function DeauthenticateTrello() {
  Trello.deauthorize();
  window.location.replace(window.location.origin + window.location.pathname);
}

function AuthenticateTrello() {
  Trello.authorize({
    name: "YourApplication",
    type: "popup",
    interactive: true,
    expiration: "1hour",
    persist: true,
    success: function () { onAuthorizeSuccessful(); },
    scope: { write: false, read: true },
  });
}

function onAuthorizeSuccessful(get_token) {
  var token = Trello.token();
  window.location.assign(window.location.href + "?token="+ token);
}

function getClass(color) {
	var css_clas = "";

	switch (color) {
		case 'green':
		css_clas = 'success';
		case 'red':
		css_clas = 'danger';
		break;
		case 'orange':
		css_clas = 'warning';
		break;
		case 'yellow':
		css_clas = 'info';
		break;
		default:
		css_clas = 'primary';
		break;
	}
	return css_clas;
}

var query_string = location.search.replace('?','').split('&');
var settings = [];
for (i = 0; i < query_string.length; i++) { 
	var temp = query_string[i].split('=');
    settings[temp[0]]=temp[1];
}



var token = settings['token'];
var board = settings['board'];
var get_enter = 'c83920bb912f2cf78cb318f98183f07f';

if(token){
	$('#cTrello').hide();
	$('#dTrello').show();
}
var link = "https://trello.com/1/boards/" + board + "/cards?key=" + get_enter + "&token="+token;

var get_lists = "https://trello.com/1/boards/" + board + "/lists?key=" + get_enter + "&token="+token;

if(board){
	var board_lists = [];
	$.get(get_lists)

  		.done(function( data ) {
    	

			$.each( data, function( key, value ) {
				board_lists[value['id']] = value['name'];
				
			});
		
		

		$.get(link)
	  	
	  		.done(function( data ) {
			
			var content = "";

			$.each( data, function( key, value ) {
				var labels_list = value['labels']
				var labels= " ";
				
				$.each( labels_list, function( key, label ) {
					
					if(label['name'] != '') {
						labels += '<span class="label label-'+ getClass(label['color'])+ '">' + label['name'] + '</span>';	
					}
					
				});
				content += '<tr><td>' + value['name'] + '</td><td>' + value['desc'].slice(0,80) + '</td><td>' +  labels  + '</td><td>' + board_lists[value['idList']] + '</td><td>' +  '<a href="' +value['shortUrl'] + '"  class="btn btn-success" role="button" target="_blank" >Open card</a>'+ '</td></tr>';
			});
		
			$('#trello').html(content);
		});
  	});
}